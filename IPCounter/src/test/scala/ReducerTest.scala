import org.apache.hadoop.io.Text
import org.mockito.Mockito.verify

import scala.collection.JavaConverters._

import hadoop.job.peripcounter.average.{AvgBytesReducer, TupleWritable}


class ReducerTest extends HadoopTest {
  "map" should  "write IP as key and custom TupleWritable with bytes and count as value" in {
    val reducer = new AvgBytesReducer
    val context = mock[reducer.Context]

    reducer.reduce(
      key=new Text("ip1187"),
      values=Seq(new TupleWritable(1500L, 6), new TupleWritable(53L, 14)).asJava,
      context=context
    )
    verify(context).write(new Text("ip1187"), new Text("77.65,1553"))
  }
}
