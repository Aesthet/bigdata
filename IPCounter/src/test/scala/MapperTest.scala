import org.apache.hadoop.io._
import org.mockito.Mockito._

import hadoop.job.peripcounter.average.{AvgBytesMapper, TupleWritable}


class MapperTest extends HadoopTest {
  "map" should  "write IP as key and custom TupleWritable with bytes and count as value" in {
    val mapper = new AvgBytesMapper
    val context = mock[mapper.Context]

    mapper.map(
      key=null,
      value=new Text("ip1187 - - [26/Apr/2011:19:15:59 -0400] \"GET /images/paper.gif HTTP/1.1\" 200 1593" +
        " \"http://host3/\" \"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0) Gecko/20100101 Firefox/4.0\""),
      context=context
    )
    verify(context).write(new Text("ip1187"), new TupleWritable(1593L, 1))
  }

  "map" should  "write IP as key and custom TupleWritable with 0 bytes and count equals to one as value" in {
    val mapper = new AvgBytesMapper
    val context = mock[mapper.Context]

    mapper.map(
      key=null,
      value=new Text("ip28 - - [24/Apr/2011:05:41:56 -0400] \"GET /sun3/ HTTP/1.1\" 304 - \"-\" \"Mozilla/5.0 " +
        "(compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)\""),
      context=context
    )
    verify(context).write(new Text("ip28"), new TupleWritable(0L, 1))
  }
}
