package hadoop.job.peripcounter.total

import java.io.{FileWriter, IOException}
import java.util.StringTokenizer

import org.apache.hadoop.io._
import org.apache.hadoop.mapreduce.Mapper


class TotalBytesMapper extends Mapper[Object, Text, Text, LongWritable] {

  val ip = new Text()
  val bytesValue = new LongWritable()

  @throws[IOException]
  override def map(key: Object, value: Text, context: Mapper[Object, Text, Text, LongWritable]#Context): Unit = {
    val pw = new FileWriter("errors.txt", true)
    val itr = new StringTokenizer(value.toString, "\n")
    while (itr.hasMoreTokens) {
      val inputString = itr.nextToken()
      val wordArray = inputString.split(" ")
      ip.set(wordArray(0))
      var bytesLong = 0L
      try {
        bytesLong = wordArray(9).toLong
      } catch {
        case error304: NumberFormatException => pw.write(inputString + "\n")
      }
      bytesValue.set(bytesLong)
      context.write(ip, bytesValue)
    }
    pw.close()
  }
}
