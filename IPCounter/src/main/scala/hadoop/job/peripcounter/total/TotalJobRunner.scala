package hadoop.job.peripcounter.total

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.Path
import org.apache.hadoop.io.{LongWritable, Text}
import org.apache.hadoop.mapreduce.Job
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat
import org.apache.hadoop.util.GenericOptionsParser

object TotalJobRunner {

  @throws[Exception]
  def main(args: Array[String]): Unit = {
    val configuration = new Configuration()
    val otherArgs = new GenericOptionsParser(configuration, args).getRemainingArgs
    if (otherArgs.length != 2) {
      println("Usage: TotalJobRunner <in> <out>")
      System.exit(2)
    }
    val job = Job.getInstance(configuration, "PerIPCountTotal")
    job.setJarByClass(TotalJobRunner.getClass)
    job.setMapperClass(classOf[TotalBytesMapper])
    job.setCombinerClass(classOf[TotalBytesReducer])
    job.setReducerClass(classOf[TotalBytesReducer])
    job.setOutputKeyClass(classOf[Text])
    job.setOutputValueClass(classOf[LongWritable])
    FileInputFormat.addInputPath(job, new Path(args(0)))
    FileOutputFormat.setOutputPath(job, new Path(args(1)))
    System.exit(if(job.waitForCompletion(true)) 0 else 1)
  }
}
