package hadoop.job.peripcounter.total

import java.io.IOException
import java.lang.Iterable

import org.apache.hadoop.io._
import org.apache.hadoop.mapreduce.Reducer

import scala.collection.JavaConverters._


class TotalBytesReducer extends Reducer[Text, LongWritable, Text, LongWritable] {

  @throws[IOException]
  override def reduce(key: Text,
                      values: Iterable[LongWritable],
                      context: Reducer[Text, LongWritable, Text, LongWritable]#Context): Unit = {
    val sum = values.asScala.foldLeft(0L)(_ + _.get)
    context.write(key, new LongWritable(sum))
  }
}
