package hadoop.job.peripcounter.average

import java.io.IOException
import java.lang.Iterable

import org.apache.hadoop.io.Text
import org.apache.hadoop.mapreduce.Reducer

import scala.collection.JavaConverters._

class AvgBytesCombiner extends Reducer[Text, TupleWritable, Text, TupleWritable] {

  @throws[IOException]
  override def reduce(key: Text,
                      values: Iterable[TupleWritable],
                      context: Reducer[Text, TupleWritable, Text, TupleWritable]#Context): Unit = {
    val sumTuples = values.asScala.foldLeft(new TupleWritable())(_+_)
    context.write(key, sumTuples)
  }
}
