package hadoop.job.peripcounter.average

import java.io.{FileWriter, IOException}
import java.util.{StringTokenizer, UnknownFormatConversionException}

import org.apache.hadoop.io._
import org.apache.hadoop.mapreduce.Mapper


class AvgBytesMapper extends Mapper[Object, Text, Text, TupleWritable] {

  val ip = new Text()
  val bytesValue = new LongWritable()
  val one = new IntWritable(1)

  @throws[IOException]
  override def map(key: Object, value: Text, context: Mapper[Object, Text, Text, TupleWritable]#Context): Unit = {
    val pw = new FileWriter("errors.txt", true)
    val itr = new StringTokenizer(value.toString, "\n")
    while (itr.hasMoreTokens) {
      val inputString = itr.nextToken()
      val wordArray = inputString.split(" ")
      ip.set(wordArray(0))
      var bytesLong = 0L
      try {
        bytesLong = wordArray(9).toLong
      } catch {
        case error304: NumberFormatException => pw.write(inputString + "\n")
        case error: Error => throw new UnknownFormatConversionException("Unknown error: " + error)
      }
      bytesValue.set(bytesLong)
      context.write(ip, new TupleWritable(bytesValue, one))
    }
    pw.close()
  }
}
