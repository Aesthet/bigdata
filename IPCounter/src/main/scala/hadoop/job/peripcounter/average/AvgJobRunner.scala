package hadoop.job.peripcounter.average

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.Path
import org.apache.hadoop.io.SequenceFile.CompressionType
import org.apache.hadoop.io.Text
import org.apache.hadoop.io.compress.SnappyCodec
import org.apache.hadoop.mapreduce.Job
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat
import org.apache.hadoop.mapreduce.lib.output.{FileOutputFormat, SequenceFileOutputFormat}
import org.apache.hadoop.util.GenericOptionsParser

object AvgJobRunner {

  @throws[Exception]
  def main(args: Array[String]): Unit = {
    val configuration = new Configuration()
    val otherArgs = new GenericOptionsParser(configuration, args).getRemainingArgs
    if (otherArgs.length != 2) {
      println("Usage: AvgJobRunner <in> <out>")
      System.exit(2)
    }
    configuration.set("mapreduce.browser_output.textoutputformat.separator", ",")
    val job = Job.getInstance(configuration, "AvgJobRunner")
    job.setJarByClass(AvgJobRunner.getClass)
    job.setMapperClass(classOf[AvgBytesMapper])
    job.setCombinerClass(classOf[AvgBytesCombiner])
    job.setReducerClass(classOf[AvgBytesReducer])
    job.setOutputKeyClass(classOf[Text])
    job.setOutputValueClass(classOf[Text])
    job.setMapOutputKeyClass(classOf[Text])
    job.setMapOutputValueClass(classOf[TupleWritable])
    FileOutputFormat.setCompressOutput(job, true)
    FileOutputFormat.setOutputCompressorClass(job, classOf[SnappyCodec])
    SequenceFileOutputFormat.setOutputCompressionType(job, CompressionType.BLOCK)

    FileInputFormat.addInputPath(job, new Path(args(0)))
    FileOutputFormat.setOutputPath(job, new Path(args(1)))
    System.exit(if(job.waitForCompletion(true)) 0 else 1)
  }
}
