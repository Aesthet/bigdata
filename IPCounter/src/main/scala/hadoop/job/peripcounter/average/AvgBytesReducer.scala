package hadoop.job.peripcounter.average

import java.io.IOException
import java.lang.Iterable

import org.apache.hadoop.io.Text
import org.apache.hadoop.mapreduce.Reducer

import scala.collection.JavaConverters._

class AvgBytesReducer extends Reducer[Text, TupleWritable, Text, Text] {

  @throws[IOException]
  override def reduce(key: Text,
                      values: Iterable[TupleWritable],
                      context: Reducer[Text, TupleWritable, Text, Text]#Context): Unit = {
    val sumTuples = values.asScala.foldLeft(new TupleWritable())(_+_)
    val array = s"${sumTuples.first.get.toFloat / sumTuples.second.get.toFloat},${sumTuples.first.get}"
    context.write(key, new Text(array))
  }
}
