package hadoop.job.peripcounter.average

import java.io.IOException
import java.io.DataInput
import java.io.DataOutput

import org.apache.hadoop.io.{IntWritable, LongWritable, Writable}


class TupleWritable(val first: LongWritable, val second: IntWritable) extends Writable with Serializable {

  def this() = this(new LongWritable(0L), new IntWritable(0))

  def this(first: LongWritable) = this(first, new IntWritable(1))

  def this(first: Long, second: Int) = this(new LongWritable(first), new IntWritable(second))

  def this(tuple: (Long, Int)) = this(new LongWritable(tuple._1), new IntWritable(tuple._2))

  @throws[IOException]
  def readFields(in: DataInput): Unit = {
    first.readFields(in)
    second.readFields(in)
  }

  @throws[IOException]
  def write(out: DataOutput): Unit = {
    first.write(out)
    second.write(out)
  }

  def +(other: TupleWritable): TupleWritable = {
    new TupleWritable(new LongWritable(this.first.get + other.first.get),
                      new IntWritable(this.second.get + other.second.get))
  }

  override def toString: String = first + "," + second

  override def hashCode: Int = first.hashCode * 163 + second.hashCode

  override def equals(other: Any): Boolean = {
    other match {
      case tw: TupleWritable => first.equals(tw.first) && second.equals(tw.second)
      case _ => false
    }
  }
}
