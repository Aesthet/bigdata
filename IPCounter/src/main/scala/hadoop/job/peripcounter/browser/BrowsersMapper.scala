package hadoop.job.peripcounter.browser

import java.io.{FileWriter, IOException}
import java.util.StringTokenizer

import org.apache.hadoop.io._
import org.apache.hadoop.mapreduce.Mapper


class BrowsersMapper extends Mapper[Object, Text, Text, IntWritable] {

  val browser = new Text()
  val one = new IntWritable(1)

  @throws[IOException]
  override def map(key: Object, value: Text, context: Mapper[Object, Text, Text, IntWritable]#Context): Unit = {
    val pw = new FileWriter("errors.txt", true)
    val itr = new StringTokenizer(value.toString, "\n")
    while (itr.hasMoreTokens) {
      val inputString = itr.nextToken()
      val wordArray = inputString.split(" ")
      try {
        browser.set{
          wordArray(11).split("/")(0) match {
            case opera if opera.contains("Opera") => "Opera"
            case chrome if chrome.contains("Chrome") => "Chrome"
            case safari if safari.contains("Safari") => "Safari"
            case ie if ie.contains("IE") => "IE"
            case mozilla if mozilla.contains("Mozilla") => "Mozilla"
            case sogou if sogou.contains("Sogou") => "Sogou"
            case baidu if baidu.contains("Baidu") => "Baidu"
            case other => "Unknown browser"
          }
        }
      } catch {
        case error: Error => pw.write(inputString + "\n")
      }
      context.write(browser, one)
    }
    pw.close()
  }
}
