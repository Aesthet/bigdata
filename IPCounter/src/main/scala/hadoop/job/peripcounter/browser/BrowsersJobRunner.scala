package hadoop.job.peripcounter.browser

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.Path
import org.apache.hadoop.io.{IntWritable, Text}
import org.apache.hadoop.mapreduce.Job
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat
import org.apache.hadoop.util.GenericOptionsParser

object BrowsersJobRunner {

  @throws[Exception]
  def main(args: Array[String]): Unit = {
    val configuration = new Configuration()
    val otherArgs = new GenericOptionsParser(configuration, args).getRemainingArgs
    if (otherArgs.length != 2) {
      println("Usage: BrowsersJobRunner <in> <out>")
      System.exit(2)
    }
    configuration.set("mapreduce.browser_output.textoutputformat.separator", ",")
    val job = Job.getInstance(configuration, "PerIPCountAverage")
    job.setJarByClass(BrowsersJobRunner.getClass)
    job.setMapperClass(classOf[BrowsersMapper])
    job.setCombinerClass(classOf[BrowsersReducer])
    job.setReducerClass(classOf[BrowsersReducer])
    job.setOutputKeyClass(classOf[Text])
    job.setOutputValueClass(classOf[IntWritable])

    FileInputFormat.addInputPath(job, new Path(args(0)))
    FileOutputFormat.setOutputPath(job, new Path(args(1)))
    System.exit(if(job.waitForCompletion(true)) 0 else 1)
  }
}
