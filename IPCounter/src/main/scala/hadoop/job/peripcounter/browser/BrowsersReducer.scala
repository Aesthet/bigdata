package hadoop.job.peripcounter.browser

import java.io.IOException
import java.lang.Iterable

import org.apache.hadoop.io.{IntWritable, Text}
import org.apache.hadoop.mapreduce.Reducer

import scala.collection.JavaConverters._

class BrowsersReducer extends Reducer[Text, IntWritable, Text, IntWritable] {

  @throws[IOException]
  override def reduce(key: Text,
                      values: Iterable[IntWritable],
                      context: Reducer[Text, IntWritable, Text, IntWritable]#Context): Unit = {
    val sum = values.asScala.foldLeft(0)(_ + _.get)
    println(s"There are $sum users of $key")
    context.write(key, new IntWritable(sum))
  }
}
