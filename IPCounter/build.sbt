scalaVersion := "2.11.12"
val scalaTest = "org.scalatest" %% "scalatest" % "3.0.5"


name := "IPCounter"
libraryDependencies += scalaTest % Test
libraryDependencies += "org.apache.hadoop" % "hadoop-common" % "3.2.0"
libraryDependencies += "org.apache.hadoop" % "hadoop-mapreduce-client-core" % "3.2.0"
libraryDependencies += "org.apache.hadoop" % "hadoop-mapreduce-client-jobclient" % "3.2.0"
libraryDependencies += "org.mockito" % "mockito-core" % "2.25.1" % Test
libraryDependencies += "org.apache.hadoop" % "hadoop-minicluster" % "3.2.0" % Test
