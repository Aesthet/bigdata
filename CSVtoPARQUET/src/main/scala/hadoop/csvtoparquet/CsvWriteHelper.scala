package hadoop.csvtoparquet

import java.util

import org.apache.hadoop.conf.Configuration
import org.apache.parquet.column.ColumnDescriptor
import org.apache.parquet.hadoop.api.WriteSupport
import org.apache.parquet.io.ParquetEncodingException
import org.apache.parquet.io.api.Binary
import org.apache.parquet.io.api.RecordConsumer
import org.apache.parquet.schema.MessageType
import parquet.schema.PrimitiveType
import org.apache.parquet.schema.PrimitiveType.PrimitiveTypeName._

import scala.util.Try


class CsvWriteHelper extends WriteSupport[util.List[String]]{

  var schema: MessageType = _
  var cols: util.List[ColumnDescriptor] = _
  var recordConsumer: RecordConsumer = _

  def this(schema: MessageType) {
    this()
    this.schema = schema
    this.cols = schema.getColumns
  }

  override def init(config: Configuration) = new WriteSupport.WriteContext(schema, new util.HashMap[String, String])

  override def prepareForWrite(r: RecordConsumer): Unit = {
    this.recordConsumer = r
  }

  override def write(values: util.List[String]): Unit = {
    if (values.size != cols.size) throw new ParquetEncodingException("Invalid input data. Expecting " + cols.size +
      " columns. Input had " + values.size + " columns (" + cols + ") : " + values)

    recordConsumer.startMessage()
    for (i <- 0 until cols.size) {
      val value = values.get(i)
      // val.length() == 0 indicates a NULL value.
      if (value.length > 0) {
        recordConsumer.startField(cols.get(i).getPath()(0), i)
        cols.get(i).getType match {
          case BOOLEAN => recordConsumer.addBoolean(value.toBoolean)
          case FLOAT => recordConsumer.addFloat(Try(value.toFloat).getOrElse(0.0F))
          case DOUBLE => recordConsumer.addDouble(Try(value.toDouble).getOrElse(0.0))
          case INT32 => recordConsumer.addInteger(Try(value.toInt).getOrElse(0))
          case INT64 => recordConsumer.addLong(Try(value.toLong).getOrElse(0L))
          case BINARY => recordConsumer.addBinary(stringToBinary(value))
          case _ => throw new ParquetEncodingException("Unsupported column type: " + cols.get(i).getPrimitiveType)
        }
        recordConsumer.endField(cols.get(i).getPath()(0), i)
      }
    }
    recordConsumer.endMessage()
  }

  private def stringToBinary(value: Any) = Try(Binary.fromString(value.toString))
    .getOrElse(Binary.fromString(""))

}
