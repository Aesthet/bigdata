package hadoop.csvtoparquet

import java.io.IOException
import java.util

import org.apache.hadoop.fs.Path
import org.apache.parquet.hadoop.ParquetWriter
import org.apache.parquet.hadoop.metadata.CompressionCodecName
import org.apache.parquet.schema.MessageType
import org.apache.parquet.hadoop.ParquetWriter.{DEFAULT_BLOCK_SIZE, DEFAULT_PAGE_SIZE}

class CsvParquetWriter(path: Path,
                       schema: MessageType,
                       complessionCodecName: CompressionCodecName,
                       enableDictionary: Boolean)
  extends ParquetWriter[util.List[String]](
    path,
    new CsvWriteHelper(schema),
    CompressionCodecName.UNCOMPRESSED,
    DEFAULT_BLOCK_SIZE,
    DEFAULT_PAGE_SIZE,
    enableDictionary,
    false) {

  @throws[IOException]
  def this(file: Path, schema: MessageType, enableDictionary: Boolean) {
    this(file, schema, CompressionCodecName.UNCOMPRESSED, enableDictionary)
  }

  @throws[IOException]
  def this(path: Path, schema: MessageType) = {
    this(path, schema, enableDictionary=false)
  }
}
