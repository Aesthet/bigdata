package hadoop.csvtoparquet

import java.io.{File, FileNotFoundException, IOException}
import java.util.regex.Pattern

import org.apache.hadoop.fs.Path
import org.apache.log4j.Logger
import org.apache.parquet.schema.{MessageType, MessageTypeParser}

import scala.collection.JavaConverters._
import scala.io.Source


object ConvertHelper {

  @transient lazy val logger: Logger = Logger.getLogger(getClass.getName)
  val CSV_DELIMITER: String = "\\s+(?=(?:[^\\'\"]*[\\'\"][^\\'\"]*[\\'\"])*[^\\'\"]*$)"

  @throws[FileNotFoundException]
  def readResource(resource: String): Iterator[String] = {
    val buf = Source.fromFile(resource)
    buf.getLines()
//    buf.close()
  }

  @throws[IOException]
  def readFile(path: String): String = {
    val ls: String = System.getProperty("line.separator")
    readResource(path).mkString(ls)
  }

  @throws[IOException]
  def getSchema(csvFile: File): String = {
    val fileName = csvFile.getName.substring(
      0, csvFile.getName.length() - ".csv".length()) + ".schema"
    val schemaFile = new File(csvFile.getParentFile, fileName)
    readFile(schemaFile.getAbsolutePath)
  }

  @throws[IOException]
  def convertCsvToParquet(csvFile: File, outputParquetFile: File) {
    convertCsvToParquet(csvFile, outputParquetFile, enableDictionary=false)
  }

  @throws[IOException]
  def convertCsvToParquet(csvFile: File, outputParquetFile: File, enableDictionary: Boolean) {
    logger.info("Converting " + csvFile.getName + " to " + outputParquetFile.getName)
    val rawSchema = getSchema(csvFile)
    if(outputParquetFile.exists()) {
      throw new IOException("Output file " + outputParquetFile.getAbsolutePath + " already exists")
    }
    val path = new Path(outputParquetFile.toURI)
    val schema: MessageType = MessageTypeParser.parseMessageType(rawSchema)
    val writer: CsvParquetWriter = new CsvParquetWriter(path, schema)

    for (line <- readResource(csvFile.toString)) {
        println(line)
        line.split(CSV_DELIMITER).foreach(println)
        val fields = line.split(CSV_DELIMITER).toList
        writer.write(fields.asJava)
      }
      writer.close()
  }
}
