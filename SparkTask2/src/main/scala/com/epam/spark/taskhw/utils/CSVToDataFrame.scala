package com.epam.spark.taskhw.utils

import org.apache.spark.sql.{DataFrame, SparkSession}


object  CSVToDataFrame {

  // Read CSV file with headers and return DataFrame with infered schema
  def apply(fileName: String): DataFrame = {
    // create spark session
    val spark = GetSparkSession(fileName.splitAt(fileName.lastIndexOf("/"))._2.stripSuffix(".csv"))

    // read csv file as DataFrame
    val df = spark.read
      .format("csv")
      .option("header", "true") // first line in file is header
      .option("inferSchema", "true") // infer schema automatically
      .option("mode", "DROPMALFORMED") // drop bad lines
      .load(fileName)

    df.printSchema()

    df
  }
}
