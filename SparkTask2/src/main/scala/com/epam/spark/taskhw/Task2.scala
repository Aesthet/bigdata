package com.epam.spark.taskhw

import com.epam.spark.taskhw.utils.CSVToDataFrame
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._
import com.epam.spark.taskhw.utils.GetSparkSession


object Task2 extends App {
  /*

 Spark application that performs query on data from csv

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 ++                                                      ++
 ++         most popular country where hotels are        ++
 ++       booked and searched from the same country      ++
 ++                                                      ++
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 */

  val file = args(0)
  val df: DataFrame = CSVToDataFrame(file)
  val spark =  GetSparkSession.get()

  def queryMostPopularCountry(initDataFrame: DataFrame): DataFrame = {
    /** Showing the most popular country where hotels are booked and searched from the same country.
      * Method takes initial dataframe as its argument, filters it accordingly, and prints result to standard output.
      */
    initDataFrame
      .filter(col("is_booking") === lit(1))
      .filter(col("user_location_country") === col("hotel_country"))
      .groupBy("hotel_country").count()
      .sort(desc("count"))
      .limit(1)
  }

  val DFToShow = queryMostPopularCountry(df)
  DFToShow.show()
  spark.stop()
}
