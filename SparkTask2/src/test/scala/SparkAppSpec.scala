import com.epam.spark.taskhw.Task2
import com.epam.spark.taskhw.utils.CSVToDataFrame
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.{DataFrame, Row}
import org.scalatest.FunSuite

class SparkAppSpec extends FunSuite {

  /*
  Tests for spark query application using scalatest
  */


  // Disable over INFO logging
  Logger.getLogger("org").setLevel(Level.ERROR)

  val file = "src/test/resources/train.csv"
  val df: DataFrame = CSVToDataFrame(file)

  test("CSVToDataFrame") {

    assert(df.isInstanceOf[DataFrame])
    assert(df.select("site_name").first() === Row(2))
  }

  test("Task2.query3MostPopularHotels") {

    val expectedResult = Array(
      Row(46, 2)
    )

    assert(Task2.queryMostPopularCountry(df).collect() === expectedResult)
  }
}
