package com.epam.streaming.task.streaming

import java.util.Properties

import com.github.tototoshi.csv._
import org.apache.kafka.clients.producer._
import org.apache.spark.sql.SparkSession


object CsvKafkaSparkHdfs {

  @throws[Exception]
  def main(args: Array[String]): Unit = {
    if (args.length != 4) {
      println("Please provide command line arguments: topicName, avroSchema, csvFile, hdfsPath")
      System.exit(-1)
    }
    val topicName = args(0)
    val avroSchema = args(1)
    val csvFile = args(2)
    val hdfsPath = args(3)

    val props = new Properties()
    props.put("bootstrap.servers", "vision:6667")
    props.put("acks", "all")
    props.put("retries", "1")
    props.put("key.serializer", "org.apache.kafka.common.serialization.IntegerSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

    val producer = new KafkaProducer[Int, String](props)

    val reader = CSVReader.open(csvFile)
    val it = reader.iterator
    // skip first 500 hundreds records
    println("Schema: " + it.next())
    var i = 1
    while (it.hasNext && i < 500) {
      it.next()
      i += 1
    }
    while (it.hasNext) {
      val record = new ProducerRecord(topicName, i, BookingRecord(it.next).toString)
      producer.send(record)
      i += 1
    }

    reader.close()
    producer.close()

    val spark = SparkSession
      .builder()
      .appName("Kafka AVRO reader")
      .getOrCreate()

    import spark.implicits._

    val df = spark
      .read
      .format("kafka")
      .option("kafka.bootstrap.servers", "vision:6667")
      .option("subscribe", topicName)
      .load()
    df.select("value")
      .as[String]
      .toJavaRDD
      .saveAsTextFile(hdfsPath)
  }
}
