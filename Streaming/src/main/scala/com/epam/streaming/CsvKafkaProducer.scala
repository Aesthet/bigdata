package com.epam.streaming


import java.util.{Properties, UUID}

import org.apache.avro.Schema
import org.apache.avro.generic.GenericData
import org.apache.avro.generic.GenericRecord
import org.apache.avro.specific.SpecificDatumWriter
import java.io.ByteArrayOutputStream

import com.github.tototoshi.csv.CSVReader
import org.apache.avro.io._
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.kafka.common.serialization.{ByteArraySerializer, StringSerializer}

import scala.io.{BufferedSource, Source}

class CsvKafkaProducer(csvFile: String, schemaFile: String, topicName: String) {
  val schemaSource: BufferedSource = Source.fromURL(getClass.getResource(schemaFile))
  val schemaString: String = schemaSource.mkString
  schemaSource.close()
  val schema: Schema = new Schema.Parser().parse(schemaString)
  private val props = new Properties()
  props.put("bootstrap.servers", "localhost:9092")
  props.put("key.serializer", classOf[StringSerializer].getCanonicalName)
  props.put("value.serializer",classOf[ByteArraySerializer].getCanonicalName)
  props.put("client.id", UUID.randomUUID().toString)
  private val producer =   new KafkaProducer[String,Array[Byte]](props)

  //Read avro schema file, read csv and send data to kafka topic
  def send(): Unit = {
    try {
        val reader = CSVReader.open(csvFile)
        val it = reader.iterator
        println("Schema: " + it.next())
        while(it.hasNext) {
        val bookingRecord = BookingRecord(it.next)
        val genericBookingRecord: GenericRecord = new GenericData.Record(schema)

        //Put data in that generic record object
        genericBookingRecord.put("date_time", bookingRecord.date_time)
        genericBookingRecord.put("site_name", bookingRecord.site_name)
        genericBookingRecord.put("posa_continent", bookingRecord.posa_continent)
        genericBookingRecord.put("user_location_country", bookingRecord.user_location_country)
        genericBookingRecord.put("user_location_region", bookingRecord.user_location_region)
        genericBookingRecord.put("user_location_city", bookingRecord.user_location_city)
        genericBookingRecord.put("orig_destination_distance", bookingRecord.orig_destination_distance)
        genericBookingRecord.put("user_id", bookingRecord.user_id)
        genericBookingRecord.put("is_mobile", bookingRecord.is_mobile)
        genericBookingRecord.put("is_package", bookingRecord.is_package)
        genericBookingRecord.put("channel", bookingRecord.channel)
        genericBookingRecord.put("srch_ci", bookingRecord.srch_ci)
        genericBookingRecord.put("srch_co", bookingRecord.srch_co)
        genericBookingRecord.put("srch_adults_cnt", bookingRecord.srch_adults_cnt)
        genericBookingRecord.put("srch_children_cnt", bookingRecord.srch_children_cnt)
        genericBookingRecord.put("srch_rm_cnt", bookingRecord.srch_rm_cnt)
        genericBookingRecord.put("srch_destination_id", bookingRecord.srch_destination_id)
        genericBookingRecord.put("srch_destination_type_id", bookingRecord.srch_destination_type_id)
        genericBookingRecord.put("is_booking", bookingRecord.is_booking)
        genericBookingRecord.put("cnt", bookingRecord.cnt)
        genericBookingRecord.put("hotel_continent", bookingRecord.hotel_continent)
        genericBookingRecord.put("hotel_country", bookingRecord.hotel_country)
        genericBookingRecord.put("hotel_market", bookingRecord.hotel_market)
        genericBookingRecord.put("hotel_cluster", bookingRecord.hotel_cluster)

        // Serialize generic record object into byte array
        val writer = new SpecificDatumWriter[GenericRecord](schema)
        val out = new ByteArrayOutputStream()
        val encoder: BinaryEncoder = EncoderFactory.get().binaryEncoder(out, null)
        writer.write(genericBookingRecord, encoder)
        encoder.flush()
        out.close()

        val serializedBytes: Array[Byte] = out.toByteArray
        println(s"Producer sending data ${serializedBytes.toString}")
        producer.send(new ProducerRecord[String, Array[Byte]](topicName, serializedBytes))
      }
    } catch {
      case ex: Exception =>
        println(ex.printStackTrace().toString)
        ex.printStackTrace()
    }
  }
}
