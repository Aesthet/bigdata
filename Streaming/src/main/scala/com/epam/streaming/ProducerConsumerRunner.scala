package com.epam.streaming

object ProducerConsumerRunner extends App {
  val topicName = args(0)
  val avroSchema = args(1)
  val csvFile = args(2)
  val hdfsPath = args(3)
  // main thread will be producer's thread
  val producer = new CsvKafkaProducer(csvFile, avroSchema, topicName)
  producer.send()

  // this thread will be consumer's thread
  val thread = new Thread {
    override def run() {
      val consumer = new KafkaHDFSConsumer(avroSchema, topicName)
      consumer.start()
    }
  }
  thread.start()
}