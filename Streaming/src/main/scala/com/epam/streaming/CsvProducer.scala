package com.epam.streaming

import java.util.Properties

import org.apache.kafka.clients.producer._
import com.github.tototoshi.csv._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._


object CsvProducer {

  @throws[Exception]
  def main(args: Array[String]):Unit = {
    if (args.length != 4) {
      println("Please provide command line arguments: topicName, avroSchema, csvFile, hdfsPath")
      System.exit(-1)
    }
    val topicName = args(0)
    val avroSchema = args(1)
    val csvFile = args(2)
    val hdfsPath = args(3)

    val props = new Properties()
    props.put("bootstrap.servers", "sandbox-hdp.hortonworks.com:6667")
    props.put("acks", "all")
    props.put("retries", "0")
    props.put("key.serializer", "io.confluent.kafka.serializers.KafkaAvroSerializer")
    props.put("value.serializer", "io.confluent.kafka.serializers.KafkaAvroSerializer")
    props.put("schema.registry.url", "sandbox-hdp.hortonworks.com:8081")

    val producer = new KafkaProducer[Int, BookingRecord](props)

    val reader = CSVReader.open(csvFile)
    val it = reader.iterator
    println("Schema: " + it.next())
    var i = 1
    while(it.hasNext) {
      val record = new ProducerRecord(topicName, i, BookingRecord(it.next))
      println(i)
      producer.send(record)
      i += 1
    }
    reader.close()
    producer.close()

    val spark = SparkSession
      .builder()
      .master("local[*]")
      .appName("Kafka AVRO reader")
      .getOrCreate()

    import spark.implicits._

    def recordToString(record: BookingRecord): String = {
      BookingRecord.toString
    }
    spark
      .readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", "localhost:9092")
      .option("subscribe", topicName)
      .load()
      .select(col("value").as[BookingRecord])
      .toJavaRDD
      .saveAsTextFile(hdfsPath)
  }
}
