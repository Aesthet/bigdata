package com.epam.streaming.custom.models

import org.apache.avro.generic.GenericRecord

case class BookingRecord(date_time: String,
                         site_name: Int,
                         posa_continent: Int,
                         user_location_country: Int,
                         user_location_region: Int,
                         user_location_city: Int,
                         orig_destination_distance: Float,
                         user_id: Int,
                         is_mobile: Int,
                         is_package: Int,
                         channel: Int,
                         srch_ci: String,
                         srch_co: String,
                         srch_adults_cnt: Int,
                         srch_children_cnt: Int,
                         srch_rm_cnt: Int,
                         srch_destination_id: Int,
                         srch_destination_type_id: Int,
                         is_booking: Int,
                         cnt: Int,
                         hotel_continent: Int,
                         hotel_country: Int,
                         hotel_market: Int,
                         hotel_cluster: Int){
  override def toString: String = {
    s"$date_time," +
      s"$site_name," +
      s"$posa_continent," +
      s"$user_location_country," +
      s"$user_location_region," +
      s"$user_location_city," +
      s"$orig_destination_distance," +
      s"$user_id," +
      s"$is_mobile," +
      s"$is_package," +
      s"$channel," +
      s"$srch_ci," +
      s"$srch_co," +
      s"$srch_adults_cnt," +
      s"$srch_children_cnt," +
      s"$srch_rm_cnt," +
      s"$srch_destination_id," +
      s"$srch_destination_type_id," +
      s"$is_booking," +
      s"$cnt," +
      s"$hotel_continent," +
      s"$hotel_country," +
      s"$hotel_market," +
      s"$hotel_cluster"
  }
}

object BookingRecord {
  def apply(list: Seq[Any]): BookingRecord = new BookingRecord(
    list.head.asInstanceOf[String],
    Integer.parseInt(list(1).toString),
    Integer.parseInt(list(2).toString),
    Integer.parseInt(list(3).toString),
    Integer.parseInt(list(4).toString),
    Integer.parseInt(list(5).toString),
    java.lang.Float.parseFloat(list(6).toString),
    Integer.parseInt(list(7).toString),
    Integer.parseInt(list(8).toString),
    Integer.parseInt(list(9).toString),
    Integer.parseInt(list(10).toString),
    list(11).asInstanceOf[String],
    list(12).asInstanceOf[String],
    Integer.parseInt(list(13).toString),
    Integer.parseInt(list(14).toString),
    Integer.parseInt(list(15).toString),
    Integer.parseInt(list(16).toString),
    Integer.parseInt(list(17).toString),
    Integer.parseInt(list(18).toString),
    Integer.parseInt(list(19).toString),
    Integer.parseInt(list(20).toString),
    Integer.parseInt(list(21).toString),
    Integer.parseInt(list(22).toString),
    Integer.parseInt(list(23).toString)
  )

  def apply(record: GenericRecord): BookingRecord = new BookingRecord(
    record.get("date_time").toString,
    Integer.parseInt(record.get("site_name").toString),
    Integer.parseInt(record.get("posa_continent").toString),
    Integer.parseInt(record.get("user_location_country").toString),
    Integer.parseInt(record.get("user_location_region").toString),
    Integer.parseInt(record.get("user_location_city").toString),
    java.lang.Float.parseFloat(record.get("orig_destination_distance").toString),
    Integer.parseInt(record.get("user_id").toString),
    Integer.parseInt(record.get("is_mobile").toString),
    Integer.parseInt(record.get("is_package").toString),
    Integer.parseInt(record.get("channel").toString),
    record.get("srch_ci").toString,
    record.get("srch_co").toString,
    Integer.parseInt(record.get("srch_adults_cnt").toString),
    Integer.parseInt(record.get("srch_children_cnt").toString),
    Integer.parseInt(record.get("srch_rm_cnt").toString),
    Integer.parseInt(record.get("srch_destination_id").toString),
    Integer.parseInt(record.get("srch_destination_type_id").toString),
    Integer.parseInt(record.get("is_booking").toString),
    Integer.parseInt(record.get("cnt").toString),
    Integer.parseInt(record.get("hotel_continent").toString),
    Integer.parseInt(record.get("hotel_country").toString),
    Integer.parseInt(record.get("hotel_market").toString),
    Integer.parseInt(record.get("hotel_cluster").toString)
  )
}
