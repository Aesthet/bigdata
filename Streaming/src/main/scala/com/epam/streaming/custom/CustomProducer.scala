package com.epam.streaming.custom

import java.util.Properties

import com.epam.streaming.custom.ApplicationConfig._
import com.epam.streaming.custom.models.BookingRecord
import com.github.tototoshi.csv.CSVReader
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}


class CustomProducer {

  val props = new Properties()

  props.put("bootstrap.servers", bootstrapServer)
  props.put("key.serializer", keySerializer)
  props.put("value.serializer", valueSerializer)

  val producer = new KafkaProducer[Int, BookingRecord](props)

  /**
    * This method will write data to given topic.
    *
    * @param topic String
    */
  def writeToKafka(topic: String, filePath: String, fileLineOffest: Int = 0) {
    val reader = CSVReader.open(csvFilePath)
    val it = reader.iterator
    // skip first $fileLineOffest records
    println("Schema: " + it.next())
    var i = 1
    while (it.hasNext && i < fileLineOffest) {
      it.next()
      i += 1
    }
    while (it.hasNext) {
      producer.send(new ProducerRecord[Int, BookingRecord](topic, i, BookingRecord(it.next)))
      i += 1
      producer.close()
    }
  }
}
