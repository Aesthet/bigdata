package com.epam.streaming.custom.serde

import java.io.{ByteArrayOutputStream, ObjectOutputStream}
import java.util.{Map => JMap}

import com.epam.streaming.custom.models.BookingRecord
import org.apache.kafka.common.serialization.Serializer

class BookingSerializer extends Serializer[BookingRecord] {

  override def configure(configs: JMap[String, _], isKey: Boolean): Unit = {
  }

  override def serialize(topic: String, data: BookingRecord): Array[Byte] = {

    try {
      val byteOutputStream = new ByteArrayOutputStream()
      val objectSerialized = new ObjectOutputStream(byteOutputStream)
      objectSerialized.writeObject(data)
      byteOutputStream.toByteArray
    }
    catch {
      case ex: Exception => throw new Exception(ex.getMessage)
    }
  }

  override def close(): Unit = {
  }

}
