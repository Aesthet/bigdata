package com.epam.streaming.custom.serde

import java.io.{ByteArrayInputStream, ObjectInputStream}
import java.util.{Map => JMap}

import com.epam.streaming.custom.models.BookingRecord
import org.apache.kafka.common.serialization.Deserializer

class BookingDeserializer extends Deserializer[BookingRecord] {


  override def configure(configs: JMap[String, _], isKey: Boolean): Unit = {
  }

  override def close(): Unit = {
  }

  override def deserialize(topic: String, bytes: Array[Byte]): BookingRecord = {

    try {
      if (bytes == null) {
        print("null received")
      }
      val byteInputStream = new ByteArrayInputStream(bytes)
      val inputObject = new ObjectInputStream(byteInputStream)
      val objectDeserialized = inputObject.readObject().asInstanceOf[BookingRecord]
      objectDeserialized
    }
    catch {
      case ex: Exception => throw new Exception(ex.getMessage)
    }
  }

}