package com.epam.streaming.custom

import com.epam.streaming.custom.ApplicationConfig._

object SolutionRunner extends App {

  (new CustomProducer).writeToKafka(topicName, csvFilePath)

  val parallelProducerDaemon = new Thread {
    override def run() {
      (new CustomProducer).writeToKafka(topicName, csvFilePath, 500)
    }
  }
  parallelProducerDaemon.start()
  parallelProducerDaemon.join()

  (new SparkConsumer).consumeRecordsAndWriteToHdfs()

}
