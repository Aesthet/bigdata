package com.epam.streaming.custom

import com.epam.streaming.custom.ApplicationConfig._
import com.epam.streaming.custom.models.BookingRecord
import org.apache.spark.sql.SparkSession

class SparkConsumer {

  def consumeRecordsAndWriteToHdfs(): Unit = {

    val spark = SparkSession
      .builder()
      .appName("Kafka AVRO reader")
      .getOrCreate()

    import spark.implicits._

    val df = spark
      .read
      .format("kafka")
      .option("kafka.bootstrap.servers", bootstrapServer)
      .option("subscribe", topicName)
      .load()
    df.selectExpr("value")
      .as[BookingRecord]
      .map(bookingRecord => bookingRecord.toString)
      .sort("key").as[Int]
      .toJavaRDD
      .saveAsTextFile(hdfsSavePath)
  }
}
