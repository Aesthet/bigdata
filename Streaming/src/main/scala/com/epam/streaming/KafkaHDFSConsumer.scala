package com.epam.streaming

import java.time.Duration
import java.util.Properties

import org.apache.avro.Schema
import org.apache.avro.io.DatumReader
import org.apache.avro.io.Decoder
import org.apache.avro.specific.SpecificDatumReader
import org.apache.avro.generic.GenericRecord
import org.apache.avro.io.DecoderFactory
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.common.errors.TimeoutException
import java.util.Collections

import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.clients.consumer.ConsumerRecords
import org.apache.kafka.common.serialization.{ByteArrayDeserializer, StringDeserializer}

import scala.io.{BufferedSource, Source}
import scala.util.{Failure, Success, Try}


class KafkaHDFSConsumer(schemaFile: String, topicName: String) {

  private val props = new Properties()
  val schemaSource: BufferedSource = Source.fromURL(getClass.getResource(schemaFile))
  val schemaString: String = schemaSource.mkString
  schemaSource.close()
  val schema: Schema = new Schema.Parser().parse(schemaString)
  var shouldRun : Boolean = true

  props.put("bootstrap.servers", "localhost:9092")
  props.put("enable.auto.commit", "true")
  props.put("auto.commit.interval.ms", "10000")
  props.put("session.timeout.ms", "30000")
  props.put("consumer.timeout.ms", "120000")
  props.put("key.deserializer", classOf[StringDeserializer].getCanonicalName)
  props.put("value.deserializer",classOf[ByteArrayDeserializer].getCanonicalName)

  private val consumer = new KafkaConsumer[String, Array[Byte]](props)

  def start(): Unit = {

    try {
      Runtime.getRuntime.addShutdownHook(new Thread() {
        close()
      })

      consumer.subscribe(Collections.singletonList(topicName))

      while (shouldRun) {
        val records: ConsumerRecords[String,  Array[Byte]] = consumer.poll(Duration.ofMinutes(2))
        val it = records.iterator()
        while(it.hasNext) {
          println("Getting message from queue.............")
          val record: ConsumerRecord[String,  Array[Byte]] = it.next()

          val bookingRecord = parseBookingRecord(record.value())
          println(s"Parsed record: $bookingRecord")
          consumer.commitSync()
        }
      }
    }
    catch {
      case timeOutEx: TimeoutException =>
        println(s"Timeout ${timeOutEx.getMessage}")
      case ex: Exception => ex.printStackTrace()
        println(s"Got error when reading message ${ex.getMessage}")
    }
  }


  private def parseBookingRecord(message: Array[Byte]): Option[BookingRecord] = {
    // Deserialize and create generic record
    val reader: DatumReader[GenericRecord] =
      new SpecificDatumReader[GenericRecord](schema)
    val decoder: Decoder = DecoderFactory.get().binaryDecoder(message, null)
    val bookingData: GenericRecord = reader.read(null, decoder)

    // Make user object
    val finalBookingRecord = Try[BookingRecord](
      BookingRecord(bookingData)
    )

    finalBookingRecord match {
      case Success(u) =>
        Some(u)
      case Failure(e) =>
        None
    }
  }

  def close(): Unit = {
    shouldRun = false
  }
}
