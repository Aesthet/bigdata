name := "Airports"

version := "0.1"

scalaVersion := "2.11.12"

val sparkVersion = "2.4.2"

libraryDependencies ++= Seq(
  // spark core
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-sql" % sparkVersion,

  // testing
  "org.scalatest" %% "scalatest" % "3.0.5" % Test,

  // logging
  "org.slf4j" % "slf4j-api" % "1.7.25",
  "org.apache.logging.log4j" % "log4j-api" % "2.11.2",
  "org.apache.logging.log4j" % "log4j-core" % "2.11.2"
)