package com.epam.spark.airports

import com.epam.spark.airports.utils.{CSVToDataFrame, GetSparkSession}
import org.apache.spark.sql.functions._
import org.apache.log4j.Logger
import org.apache.spark.sql.DataFrame
import org.apache.log4j.Level


/*
Spark HW:
1.	Use dataset http://stat-computing.org/dataexpo/2009/the-data.html (year 2007 + dimensions http://stat-computing.org/dataexpo/2009/supplemental-data.html only Airports and Carrier Codes).
2.	Load data to DataFrames (make #1 screenshot with samples from dataframe(s))
3.	Count total number of flights per carrier in 2007 (make #2 screenshot)
4.	The total number of flights served in Jun 2007 by NYC (all airports, use join with Airports data and don’t forget make screenshot #3
5.	Find five most busy airports in US during Jun 01 - Aug 31 (make #4).
6.	Find the carrier who served the biggest number of flights (make #5)

In Spark HW should be
•	Readme.txt with building/deploy instruction
•	Spark project with tests
•	5 screenshots
 */
object Airports extends App {

  Logger.getLogger("org").setLevel(Level.OFF)
  Logger.getLogger("akka").setLevel(Level.OFF)

  @transient lazy val logger = Logger.getLogger(getClass.getName)

  val year2008Df = CSVToDataFrame("Airports/src/main/resources/2008.csv.bz2")
  val airportsDf = CSVToDataFrame("Airports/src/main/resources/airports.csv")
  val carriersDf = CSVToDataFrame("Airports/src/main/resources/carriers.csv")

  year2008Df.sample(0.0005).show(5)
  airportsDf.sample(0.001).show(5)
  carriersDf.sample(0.005).show(5)

  val year2008DfPlusCarriersDf = year2008Df
    .join(carriersDf, year2008Df("UniqueCarrier") === carriersDf("Code"), "left")
  year2008DfPlusCarriersDf.cache()

  val spark = GetSparkSession("Airports and flights")

  def getFlightsPerCarrierIn2007(initRDD: DataFrame): DataFrame = {
    val result = initRDD
      .groupBy("Description").count()
    result
  }

  logger.info("getFlightsPerCarrierIn2007 Result: " +
    getFlightsPerCarrierIn2007(year2008DfPlusCarriersDf).toJavaRDD.take(3))

  def getCarrierWithMostFlights(initRDD: DataFrame): String = {
    val result = getFlightsPerCarrierIn2007(initRDD)
      .sort(col("count").desc)
      .take(1)
    result.toList.head.getAs[String]("Description")
  }
  logger.info("getCarrierWithMostFlights Result: " +
    getCarrierWithMostFlights(year2008DfPlusCarriersDf))

  val year2008DfPlusAirportsDf = year2008Df.join(airportsDf, year2008Df("Origin") === airportsDf("iata"),
    "left")
  year2008DfPlusAirportsDf.cache()

  def getFlightsByNYCInJuly(initRDD: DataFrame): DataFrame = {
    val result = initRDD
      .filter(col("Month") === lit(7))
      .groupBy(col("City")).count()
      .filter(col("City") === lit("New York"))
    result
  }
  logger.info("getFlightsByNYCInJuly Result: " +
    getFlightsByNYCInJuly(year2008DfPlusAirportsDf).toJavaRDD.take(1))

  def getFlightsByUSAInSummer(initRDD: DataFrame): DataFrame = {
    val result = initRDD
      .filter(col("country") === lit("USA"))
      .filter(col("Month").between(6, 8))
      .groupBy(col("airport"))
      .count()
      .sort(col("count").desc)
    result
  }
  logger.info("getFlightsByUSAInSummer Result: " +
    getFlightsByUSAInSummer(year2008DfPlusAirportsDf).toJavaRDD.take(3))

  spark.stop()
}
