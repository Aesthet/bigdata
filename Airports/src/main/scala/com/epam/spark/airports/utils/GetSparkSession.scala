package com.epam.spark.airports.utils

import org.apache.spark.sql.SparkSession

// Singleton instance of spark session
object GetSparkSession {

    @transient  private var spark: SparkSession = _

    def apply(appName: String): SparkSession = {
      if (spark == null) {
        spark = SparkSession
          .builder()
          .master("local[*]")
          .appName(appName)
          .getOrCreate()
      }
      spark
    }

  def get(): SparkSession = {
    spark
  }
}
