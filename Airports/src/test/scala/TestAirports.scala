import com.epam.spark.airports.Airports
import com.epam.spark.airports.utils.CSVToDataFrame

import org.apache.spark.sql.{DataFrame, Row}
import org.scalatest.FunSuite

class TestAirports extends FunSuite {

  /*
  Tests for spark query application using scalatest
  */

  val year2008Df = CSVToDataFrame("src/main/resources/2008.csv.bz2")
  val airportsDf = CSVToDataFrame("src/main/resources/airports.csv")
  val carriersDf = CSVToDataFrame("src/main/resources/carriers.csv")
  val year2008DfPlusAirportsDf: DataFrame = year2008Df.join(airportsDf, year2008Df("Origin") === airportsDf("iata"),
    "left")
  val year2008DfPlusCarriersDf: DataFrame = year2008Df
    .join(carriersDf, year2008Df("UniqueCarrier") === carriersDf("Code"), "left")

  test("CSVToDataFrame") {

    assert(year2008Df.isInstanceOf[DataFrame])
    assert(year2008Df.select("DepTime").first() === Row("2003"))
    assert(year2008Df.select("TailNum").first() === Row("N712SW"))
  }


  test("Airports.getFlightsPerCarrierIn2007") {
    val expectedResult = Array(
      Row("US Airways Inc. (Merged with America West 9/05. Reporting for both starting 10/07.)", 453589),
      Row("Pinnacle Airlines Inc.", 262208),
      Row("Aloha Airlines Inc.", 7800)
    )

    assert(Airports.getFlightsPerCarrierIn2007(year2008DfPlusCarriersDf).take(3) === expectedResult)
  }

  test("Airports.getCarrierWithMostFlights") {
    val expectedResult = "Southwest Airlines Co."

    assert(Airports.getCarrierWithMostFlights(year2008DfPlusCarriersDf) === expectedResult)
  }

  test("Airports.getFlightsByNYCInJuly") {
    val expectedResult = Array(
      Row("New York", 22020)
    )

    assert(Airports.getFlightsByNYCInJuly(year2008DfPlusAirportsDf).take(1) === expectedResult)
  }

  test("Airports.getFlightsByUSAInSummer") {
    val expectedResult = Array(
      Row("William B Hartsfield-Atlanta Intl", 107382),
      Row("Chicago O'Hare International", 92820),
      Row("Dallas-Fort Worth International", 74993)
    )

    assert(Airports.getFlightsByUSAInSummer(year2008DfPlusAirportsDf).take(3) === expectedResult)
  }
}
