package com.epam.spark.taskhw

import com.epam.spark.taskhw.utils.{CSVToDataFrame, GetSparkSession}
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._


object Task1 extends App {
  /*

  Spark application that performs query on data from csv

  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  ++                                                      ++
  ++         3 most popular hotels among couples          ++
  ++                                                      ++
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  */

  val file = args(0)
  val df: DataFrame = CSVToDataFrame(file)
  val spark =  GetSparkSession.get()

  // udf to treat hotel as composite key of 3 fields: continent, country and market
  val concatHotel = udf {
    (continent: Int, country: Int, market: Int) => {
      s"$continent, $country, $market"
    }
  }

  def query3MostPopularHotels(initDataFrame: DataFrame): DataFrame = {
    /** Showing  top 3 most popular hotels between couples. (hotel is treated as composite key of continent, country
      * and market).
      * Method takes initial dataframe as its argument, filters it accordingly, and prints result to standard output.
      */

    initDataFrame
      .select("hotel_continent", "hotel_country", "hotel_market")
      .filter(col("srch_adults_cnt") === lit(2))
      .groupBy("hotel_continent", "hotel_country", "hotel_market")
      .count()
      .sort(col("count").desc)
      .limit(3)
  }

  val DFToShow = query3MostPopularHotels(df)
  DFToShow.show()
  spark.stop()
}
