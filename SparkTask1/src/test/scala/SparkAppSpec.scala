import com.epam.spark.taskhw.Task1
import com.epam.spark.taskhw.utils.CSVToDataFrame
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.{DataFrame, Row}
import org.scalatest.FunSuite

class SparkAppSpec extends FunSuite {

  /*
  Tests for spark query application using scalatest
  */


  // Disable over INFO logging
  Logger.getLogger("org").setLevel(Level.OFF)

  val file = "src/test/resources/train.csv"
  val df: DataFrame = CSVToDataFrame(file)

  test("CSVToDataFrame") {

    assert(df.isInstanceOf[DataFrame])
    assert(df.select("site_name").first() === Row(2))
  }

  test("Task1.query3MostPopularHotels") {

    val expectedResult = Array(
      Row(6, 105, 29, 91),
      Row(2, 50, 368, 83),
      Row(6, 105, 35, 80)
    )
    val DFToShow = Task1.query3MostPopularHotels(df)
    DFToShow.show(3)

    assert(Task1.query3MostPopularHotels(df).collect() === expectedResult)
  }
}
