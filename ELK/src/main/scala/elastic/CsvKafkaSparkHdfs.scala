package elastic

import java.util.Properties
import org.elasticsearch.spark.sql._
import com.github.tototoshi.csv._
import org.apache.kafka.clients.producer._
import org.apache.spark.sql.SparkSession

object CsvKafkaSparkHdfs {

  @throws[Exception]
  def main(args: Array[String]): Unit = {
    if (args.length != 2) {
      println("Please provide command line arguments: topicName, csvFile")
      System.exit(-1)
    }
    val topicName = args(0)
    val csvFile = args(1)

    val props = new Properties()
    props.put("bootstrap.servers", "vision:6667")
    props.put("acks", "all")
    props.put("retries", "1")
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

    val producer = new KafkaProducer[String, String](props)

    val reader = CSVReader.open(csvFile)
    val it = reader.iterator
    // skip first 500 hundreds records
    println("Schema: " + it.next())
    var i: Int = 1
    while (it.hasNext) {
      val record = new ProducerRecord(topicName, i.toString, BookingRecord(it.next).toString)
      producer.send(record)
      i += 1
    }

    reader.close()
    producer.close()

    val spark = SparkSession
      .builder()
      .appName("EKL streaming")
      .config("spark.es.nodes", "127.0.0.1")
      .config("spark.es.port", 9200)
      .config("spark.es.index.auto.create", "true")
      .getOrCreate()

    import spark.implicits._

    def unBox(string: String): BookingRecord = {
      BookingRecord(string.split(","))
    }
    val df = spark
      .read
      .format("kafka")
      .option("kafka.bootstrap.servers", "vision:6667")
      .option("subscribe", topicName)
      .load()

    df.select("value")
      .as[String]
      .map(unBox)
      .saveToEs("trainindex/docs")
  }
}
