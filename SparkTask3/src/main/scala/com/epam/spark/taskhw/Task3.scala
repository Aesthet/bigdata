package com.epam.spark.taskhw

import com.epam.spark.taskhw.utils.{CSVToDataFrame, GetSparkSession}
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions._


object Task3 extends App {
  /*

  Spark application that performs query on data from csv

  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  ++                                                      ++
  ++       top 3 hotels where people with children        ++
  ++       are interested but not booked in the end       ++
  ++                                                      ++
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  */

  val file = args(0)
  val df: DataFrame = CSVToDataFrame(file)

  val spark =  GetSparkSession.get()

  // udf to treat hotel as composite key of 3 fields: continent, country and market
  val concatHotel = udf {
    (continent: Int, country: Int, market: Int) => {
      s"$continent, $country, $market"
    }
  }

  def popularNotBookedHotels(initDataFrame: DataFrame): DataFrame = {
    /** Showing top 3 hotels where people with children are interested but not booked in the end.
      * Method takes initial dataframe as its argument, filters it accordingly, and prints result to standard output.
      */
    initDataFrame
      .select("hotel_continent", "hotel_country", "hotel_market")
      .filter(col("is_booking") === lit(0))
      .filter(col("srch_adults_cnt") > lit(1))
      .filter(col("srch_children_cnt") > lit(1))
      .groupBy("hotel_continent", "hotel_country", "hotel_market")
      .count()
      .sort(desc("count"), asc("hotel_continent"))
      .limit(3)
  }

  val DFToShow = popularNotBookedHotels(df)
  DFToShow.show()
  spark.stop()
}
