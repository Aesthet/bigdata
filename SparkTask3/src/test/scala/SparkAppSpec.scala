import com.epam.spark.taskhw.Task3
import com.epam.spark.taskhw.utils.CSVToDataFrame
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.{DataFrame, Row}
import org.scalatest.FunSuite


class SparkAppSpec extends FunSuite {

  /*
  Tests for spark query application using scalatest
  */


  // Disable over INFO logging
  Logger.getLogger("org").setLevel(Level.ERROR)

  val file = "src/test/resources/train.csv"
  val df: DataFrame = CSVToDataFrame(file)

  test("CSVToDataFrame") {

    assert(df.isInstanceOf[DataFrame])
    assert(df.select("site_name").first() === Row(2))
  }

  test("Task3.popularNotBookedHotels") {

    val expectedResult = Array(
      Row(4, 163, 1503, 4),
      Row(6, 204, 1776, 4),
      Row(2, 50, 1230, 3)
    )

    assert(Task3.popularNotBookedHotels(df).collect() === expectedResult)
  }
}
