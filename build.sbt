ThisBuild / scalaVersion := "2.11.12"
ThisBuild / organization := "com.epam"

val scalaTest = "org.scalatest" %% "scalatest" % "3.0.5" % Test

lazy val bigdata = (project in file("."))
  .settings(
    name := "SBT Sub Projects",
    version := "1.0",
    libraryDependencies += scalaTest
  )

lazy val IPCounter = (project in file("IPCounter"))
  .settings(
    name := "IPCounter",
    resolvers += "Artima Maven Repository" at "http://repo.artima.com/releases",
    libraryDependencies += scalaTest,
    libraryDependencies += "org.apache.hadoop" % "hadoop-common" % "3.2.0",
    libraryDependencies += "org.apache.hadoop" % "hadoop-mapreduce-client-core" % "3.2.0",
    libraryDependencies += "org.apache.hadoop" % "hadoop-mapreduce-client-jobclient" % "3.2.0",
    libraryDependencies += "org.mockito" % "mockito-core" % "2.25.1" % Test,
    libraryDependencies += "org.apache.hadoop" % "hadoop-minicluster" % "3.2.0" % Test
  )

lazy val CSVtoPARQUET = (project in file("CSVtoPARQUET"))
  .settings(
    name := "CSVtoPARQUET",
    libraryDependencies += scalaTest,
    libraryDependencies += "log4j" % "log4j" % "1.2.17",
    libraryDependencies += "org.apache.hadoop" % "hadoop-common" % "3.2.0",
    libraryDependencies += "org.apache.parquet" % "parquet-common" % "1.10.1",
    libraryDependencies += "org.apache.parquet" % "parquet-hadoop" % "1.10.1",
    libraryDependencies += "org.apache.parquet" % "parquet-column" % "1.10.1",
    libraryDependencies += "com.twitter" % "parquet-hadoop" % "1.6.0",
    libraryDependencies += "com.twitter" % "parquet-column" % "1.6.0"
  )

val sparkVersion = "2.4.2"

lazy val Airports = (project in file("Airports"))
  .settings(
      name := "Airports",
      libraryDependencies += scalaTest,
      libraryDependencies += "log4j" % "log4j" % "1.2.17",
      libraryDependencies += "org.apache.spark" %% "spark-core" % sparkVersion,
      libraryDependencies += "org.apache.spark" %% "spark-sql" % sparkVersion
  )

lazy val SparkTask1 = (project in file("SparkTask1"))
  .settings(
    name := "SparkTask1",
    libraryDependencies += scalaTest,
    libraryDependencies += "log4j" % "log4j" % "1.2.17",
    libraryDependencies += "org.apache.spark" %% "spark-core" % sparkVersion,
    libraryDependencies += "org.apache.spark" %% "spark-sql" % sparkVersion
  )

lazy val SparkTask2 = (project in file("SparkTask2"))
  .settings(
    name := "SparkTask2",
    libraryDependencies += scalaTest,
    libraryDependencies += "log4j" % "log4j" % "1.2.17",
    libraryDependencies += "org.apache.spark" %% "spark-core" % sparkVersion,
    libraryDependencies += "org.apache.spark" %% "spark-sql" % sparkVersion
  )

lazy val SparkTask3 = (project in file("SparkTask3"))
  .settings(
    name := "SparkTask3",
    libraryDependencies += scalaTest,
    libraryDependencies += "log4j" % "log4j" % "1.2.17",
    libraryDependencies += "org.apache.spark" %% "spark-core" % sparkVersion,
    libraryDependencies += "org.apache.spark" %% "spark-sql" % sparkVersion
  )

lazy val Streaming = (project in file("Streaming"))
  .settings(
    name := "Streaming",
    libraryDependencies ++= Seq(
      "org.apache.spark" %% "spark-sql" % sparkVersion % "provided",
      "org.apache.spark" %% "spark-sql-kafka-0-10" % sparkVersion,
      "com.github.tototoshi" %% "scala-csv" % "1.3.5",
      "com.typesafe" % "config" % "1.3.4"
    )
  )

lazy val ELK = (project in file("ELK"))
  .settings(
    name := "ELK",
    libraryDependencies ++= Seq(
      "org.apache.spark" %% "spark-sql" % sparkVersion % "provided",
      "org.apache.spark" %% "spark-sql-kafka-0-10" % sparkVersion,
      "com.github.tototoshi" %% "scala-csv" % "1.3.5",
      "com.typesafe" % "config" % "1.3.4",
      "org.elasticsearch" %% "elasticsearch-spark-20" % "7.1.1"
    )
  )